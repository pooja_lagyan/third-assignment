package com.producerconsumer.main;
import com.producerconsumer.entities.*;
import java.util.ArrayList;
import java.util.Scanner;

public class MainClass 
{    
    public static void main(String[] args)
    {
        Scanner obj = new Scanner(System.in);
        ArrayList<Integer> sharedQueue = new ArrayList<Integer>();
        int size = 20;
        String key = new String();
        
        Producer producer = new Producer(sharedQueue, size); 
        Consumer consumer = new Consumer(sharedQueue, size);
        Thread prodThread = new Thread(producer, "Producer");
        Thread consThread = new Thread(consumer, "Consumer");
        
        prodThread.start();
        consThread.start();
        
        System.out.println("Press return key to exit : ");
        key = obj.nextLine();
        
        producer.shutdown();
        consumer.shutdown();
    }
}
