package com.producerconsumer.entities;
import java.util.ArrayList;
import java.util.Random;

public class Producer implements Runnable
{
    public int random;
    Random randomGenerator = new Random();
    ArrayList<Integer> sharedQueue;
    private final int SIZE;
    private boolean running = true;
    public Producer(ArrayList<Integer> sharedQueue, int size) 
    {
        this.sharedQueue = sharedQueue;
        this.SIZE = size;
    }

    @Override
    synchronized public void run() 
    {
        while(running)
        {
            random = randomGenerator.nextInt(100);
            System.out.println("Produced: " + random);
            try 
            {
                produce(random++);
            } 
            catch (InterruptedException e) 
            {
                System.out.println(e+" - Exception Found");
            }
        
        }
    }
    
    
    private void produce(int i) throws InterruptedException 
    {
        //wait if queue is full
        while (sharedQueue.size() == SIZE) 
        {
            synchronized (sharedQueue) 
            {
                System.out.println("Queue is full " + Thread.currentThread().getName()
                                    + " is waiting , size: " + sharedQueue.size());
                sharedQueue.wait();
            }
        }
        synchronized (sharedQueue) 
        {
                sharedQueue.add(i);
                sharedQueue.notifyAll();
        }
    }
    
     public void shutdown() {
        running = false;
    }
}
    
 
