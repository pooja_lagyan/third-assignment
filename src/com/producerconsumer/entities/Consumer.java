package com.producerconsumer.entities;
import java.util.ArrayList;

public class Consumer implements Runnable
{
    ArrayList<Integer> sharedQueue;
    private final int SIZE;
    private boolean running = true;
    
    public Consumer( ArrayList<Integer> sharedQueue, int size) 
    {
        this.sharedQueue = sharedQueue;
        this.SIZE = size;
    }

    @Override
    public void run() 
    {
        while (running) 
        {
            try 
            {
                System.out.println("Consumed: " + consume());
            } 
            catch (InterruptedException e) 
            {
               System.out.println(e+" - Exception Found");
            }

        }
    }

    private int consume() throws InterruptedException 
    {
        //wait if queue is empty
        
        while (sharedQueue.isEmpty()) 
        {
            synchronized (sharedQueue) 
            {
                System.out.println("Queue is empty " + Thread.currentThread().getName()+ " is waiting , size: " + sharedQueue.size());
                sharedQueue.wait();
            }
        }

        //Otherwise consume element and notify waiting producer
        
        synchronized (sharedQueue) 
        {
            sharedQueue.notifyAll();
            return (Integer)sharedQueue.remove(0);
        }
    }
    
     public void shutdown() 
     {
        running = false;
     }

 }
